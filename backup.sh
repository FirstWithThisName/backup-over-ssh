#!/bin/bash
BACKUP_HOST="host.to.backup"
BACKUP_USER="backup-user"
BACKUP_SRC="/var/backup/path"

BACKUP_DIR=$(date +"%d.%m.%Y")

echo "Creating backup dir $BACKUP_DIR"
if ! mkdir $HOME/$BACKUP_DIR; then
	echo "Creation of backup dir failed!"
	exit -1
fi

# copy files from host
echo "Copy $BACKUP_USER@$BACKUP_HOST:$BACKUP_SRC to $HOME/$BACKUP_DIR"
if ! scp -C -r $BACKUP_USER@$BACKUP_HOST:$BACKUP_SRC $HOME/$BACKUP_DIR; then
	echo "Copying failed!"
	exit -2
fi

# compressing backup dir
echo "Compressing $HOME/$BACKUP_DIR to $HOME/$BACKUP_DIR.tar.gz" 
if ! tar -zcvf "$HOME/$BACKUP_DIR.tar.gz" "$HOME/$BACKUP_DIR"; then
	echo "Compressing failed"
	exit -3
fi

# removing backup dir
echo "Removing $HOME/$BACKUP_DIR"
if ! rm -rf $HOME/$BACKUP_DIR; then
	echo "Removing of dir failed"
	exit -4
fi
